package com.jimson.pierrelouis;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button button_autre;
    private static final String TAG = "DEBUG";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button_autre=findViewById(R.id.autre);

        button_autre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lancerActivite();
            }
        });
    }

    private void lancerActivite() {
        Context context = getBaseContext();
        // Intention explicite
        Intent intent = new Intent(context, AutreActivity.class);
        // Lancer l'entention pour afficher la vue de destination
        startActivity(intent);
        Log.d(TAG, "Vous avez cliqué sur le bouton sur le bouton 1");
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "La méthode onStart est appelée");
    }


    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG, "La méthode onResume est appelée");
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d(TAG, "La méthode onPause est appelée");
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.d(TAG, "La méthode onStop est appelée");
    }

    @Override
    public void onRestart(){
        super.onRestart();
        Log.d(TAG, "La méthode onRestart() est appelée");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "La méthode onDestroy est appelée");
    }


}