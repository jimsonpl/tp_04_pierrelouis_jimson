package com.jimson.pierrelouis;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class AutreActivity extends AppCompatActivity {

    private Button button_fermer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent=new Intent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autre);

        button_fermer=findViewById(R.id.fermer);
        Fermer();
    }


    public  void Fermer(){
        Context context = getBaseContext();
        Intent intent = new Intent(context, MainActivity.class);
        button_fermer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
